var btnEmpezar = document.getElementById('btnEmpezar');
var btnReiniciar = document.getElementById('btnReiniciar');
var scoreboard = document.getElementById('scoreboard').getElementsByTagName("li");

var btnPiedra = document.getElementById("btnPiedra");
var btnPapel = document.getElementById("btnPapel");
var btnTijera = document.getElementById("btnTijera");

var mensaje = document.getElementById("mensaje");
var cpuSide = document.getElementById("cpu-side");
var userSide = document.getElementById("user-side");

var cpuScore = document.getElementById("cpu-score");
var userScore = document.getElementById("user-score");

var endScreen = document.getElementById("end-screen");
var victorySound = document.getElementById("victory-sound");
var defeatSound = document.getElementById("defeat-sound");

var partidaEnCurso; // Cambia a false si ya se definió un ganador.

// [usuario, computadora]
var partidas = [0,0]; // (NO RESETEABLE) Tabla de partidas ganadas.
var puntajes = [0,0]; // Tabla de rondas ganadas en la presente partida.

var rondas = 5, rondaActual;
var conteoLimite = 6, conteoActual;

var usuario, computadora, interval;

function inicializar() {
	limpiarBotones();
	desactivarBotonera();
	mostrarCpuSideImg(0);
	partidaEnCurso = true;
	rondaActual = 0;
	puntajes = [0,0];
	btnEmpezar.innerHTML = "Jugar!";
	mensaje.innerHTML = "";
}

/* Rehabilita los botones y la decisión del usuario 
 * respecto a la ronda anterior.
 * * * * * * * * * * * * * * * * * * * * * *  * * */
function nuevaRonda() {
	if (partidaEnCurso) {
		rondaActual++;
		usuario = null;
		btnEmpezar.disabled = false;
		limpiarBotones();
		activarBotonera();
		empezarRonda();
	}
	else {
		console.log("Reiniciando!");
		limpiarTablaDePuntajes();
		inicializar();
	}
}

// Inicia el conteo.
function empezarRonda() {
	btnEmpezar.disabled = true;
	conteoActual = conteoLimite;
	btnEmpezar.innerHTML = conteoActual;

	interval = setInterval(function() {
		btnEmpezar.innerHTML = --conteoActual;
		if (conteoActual <= 0) {
			terminarRonda();
		}
	}, 1000);
}



/* NOTA:
 * 		1 - Piedra
 *		2 - Papel
 *		3 - Tijera
 */

function terminarRonda() {
	btnEmpezar.disabled = false;
	mensaje.innerHTML = "";
	btnEmpezar.innerHTML = "Nueva ronda";
	clearInterval(interval);
	computadora = obtenerResultado();

	/*
	 * Para prevenir la redundancia, utilizo el método de la recursividad
	 * para tratar de obtener un resultado distinto.
	 */
	if (usuario == computadora)
		return terminarRonda();
	else {
		switch(usuario) {

			case 1: // Piedra
				if (computadora == 2) 
					declararGanadorRonda("cpu");
				else declararGanadorRonda("user");
				break;

			case 2: // Papel
				if (computadora == 3)
					declararGanadorRonda("cpu");
				else declararGanadorRonda("user");
				break;

			case 3: // Tijera
				if (computadora == 1)
					declararGanadorRonda("cpu");
				else declararGanadorRonda("user");
				break;
		}
	}

	mostrarCpuSideImg(computadora);
	desactivarBotonera();

	if (puntajes[0] > rondas / 2)
		declararGanadorJuego('user');
	else if (puntajes[1] > rondas / 2)
		declararGanadorJuego('cpu');

}

function declararGanadorRonda(side) {
	var perdedor = document.createElement("div");
	perdedor.setAttribute('class', 'col-auto');
	var ganador = document.createElement("div");
	ganador.setAttribute('class', 'col-auto win');

	switch (side){
		case "user": puntajes[0]++;
					 userScore.appendChild(ganador);
					 cpuScore.appendChild(perdedor);
					 mensaje.innerHTML = "<strong>Usuario</strong> gana la ronda!";
					 break;
		case "cpu": puntajes[1]++;
					cpuScore.appendChild(ganador);
					userScore.appendChild(perdedor);
					mensaje.innerHTML = "<strong>Computadora</strong> gana la ronda!";
					// userScore.appendChild("<div class='col'></div>");
					break;
		
	}
}


function declararGanadorJuego(side) {
	switch (side) {
		case 'user': 
			console.log("GANADOR: USUARIO");
			mensaje.innerHTML = "GANADOR: USUARIO";
			victorySound.play();
			scoreboard[0].innerHTML = "Usuario: " + (++partidas[0]);
			break;
		case 'cpu':
			console.log("GANADOR: COMPUTADORA");
			mensaje.innerHTML = "GANADOR: COMPUTADORA";
			defeatSound.play();
			scoreboard[1].innerHTML = "Computadora: " + (++partidas[1]);
			break;
	}
	desactivarBotonera();
	partidaEnCurso = false;
	btnEmpezar.innerHTML = "NUEVA PARTIDA";
}

function mostrarCpuSideImg(index) {
	var cpuSideImg = cpuSide.getElementsByTagName('img');
	for (var i = 0; i < cpuSideImg.length; i++) {
		if (i == index) {
			if (!cpuSideImg[i].classList.contains("show"))
			cpuSideImg[i].classList.add("show");
		}
		else {
			if (cpuSideImg[i].classList.contains("show"))
			cpuSideImg[i].classList.remove("show");
		}
	}
}



function obtenerResultado() {
	var rand = Math.random();
	 
	if (rand < 0.33)
		return 1; // Piedra
	else if (rand > 0.33 && rand < 0.66)
		return 2; // Papel
	else 
		return 3; // Tijera
}


function desactivarBotonera() {
	if (!userSide.classList.contains("show-selected"))
		userSide.classList.add("show-selected");
}
function activarBotonera() {
	if (userSide.classList.contains("show-selected"))
		userSide.classList.remove("show-selected");
}

function limpiarBotones() {
	if (btnPiedra.classList.contains("btnSelected"))
		btnPiedra.classList.remove("btnSelected");
	if (btnPapel.classList.contains("btnSelected"))
		btnPapel.classList.remove("btnSelected");
	if (btnTijera.classList.contains("btnSelected"))
		btnTijera.classList.remove("btnSelected");
}


function limpiarTablaDePuntajes() {
	for (var i = 0; i < rondaActual; i++){
		userScore.getElementsByTagName('div')[0].remove();
		cpuScore.getElementsByTagName('div')[0].remove();
	}
}

/*
** EVENTOS
*/

window.addEventListener("load", inicializar);

btnEmpezar.addEventListener("click", nuevaRonda);

btnPiedra.addEventListener("click", function() {
	if (!userSide.classList.contains("show-selected")) {
		usuario = 1; // Piedra
		btnPiedra.classList.add("btnSelected");
		console.log("%c EXITO %c: usuario eligió " + usuario, "background: lime;", "background: transparent;");
		terminarRonda();
	}
});

btnPapel.addEventListener("click", function() {
	if (!userSide.classList.contains("show-selected")) {
		usuario = 2; // Papel
		btnPapel.classList.add("btnSelected");
		console.log("%c EXITO %c: usuario eligió " + usuario, "background: lime;", "background: transparent;");
		terminarRonda();
	}
});

btnTijera.addEventListener("click", function() {
	if (!userSide.classList.contains("show-selected")) {
		usuario = 3; // Tijera
		btnTijera.classList.add("btnSelected");
		console.log("%c EXITO %c: usuario eligió " + usuario, "background: lime;", "background: transparent;");
		terminarRonda();
	}
});


btnReiniciar.addEventListener("click", function() {
	
	scoreboard[0].innerHTML = "Usuario: 0";
	scoreboard[1].innerHTML = "Computadora: 0";
	partidas = [0,0];
	
});